import { applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {createLogger} from "redux-logger";
import reducers from './reducers';

const stringUser = localStorage.getItem('user');
const jsonUser = stringUser ? JSON.parse(stringUser) : null;

const middleware = applyMiddleware(thunk, createLogger());

const store = createStore(reducers, {
    user: jsonUser ? jsonUser : {}
}, middleware)

export default store;