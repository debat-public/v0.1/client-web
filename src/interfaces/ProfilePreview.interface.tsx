interface ProfilePreviewInterface {
    id?: string;
    firstName?: string;
    lastName?: string;
    job?: string;
    bio?: string;
}

export default ProfilePreviewInterface;