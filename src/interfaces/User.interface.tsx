/**
 * To Complete
 */
interface UserInterface {
  id?: string;
  email?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  createdAt?: Date;
  bio?: string; // presentation
  karma?: Array<string>; // user ids who gave a karma
}
export default UserInterface;
