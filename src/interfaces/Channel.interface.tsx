/**
 * To Complete
 */
interface Channel {
  id?: string;
  uid?: string;               // user id 
  title?: string
  createdAt?: Date;
}
export default Channel;