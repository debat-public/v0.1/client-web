/**
 * To Complete
 */
interface Project {
  id?: string;
  message?: string
  channelId?: string;               // channel id 
  reactionsId?: Array<string>;  // reaction user ids
  commentsId?: Array<string>;
  createdBy?: string;         // user id 
  createdAt?: Date;
  modifiedAt?: Date;
}
export default Project;