interface ProjectPreviewInterface {
    id?: string;
    title: string;
    description: string;
    date: string;
}


export default ProjectPreviewInterface;