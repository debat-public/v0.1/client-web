import ReactionInterface from "./Reaction.interface";
import ProjectPreviewInterface from "./ProjectPreview.interface";

interface ProfileInterface {
  firstName?: string;
  lastName?: string;
  email?: string;
  age?: Date;
  bio?: string;
  photo?: string;
  career?: string;
  listReaction?: Array<ReactionInterface>;
  listProjects?: Array<ProjectPreviewInterface>;
}

export default ProfileInterface;
