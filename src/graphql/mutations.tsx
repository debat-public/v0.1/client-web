import gql from "graphql-tag";

/**
 *  Mutation file
 */
export const CREATE_OPINION = gql`
  mutation CreateOpinion($title: String!, $description: String!, $idUser: String!, $idProject: String!){
    CreateOpinion(title: $title, description: $description, idUser: $idUser, idProject: $idProject) {
      title
      description
    }
  }
`;

export const CREATE_REACTION = gql`
  mutation CreateReaction($title: String!, $idUser: String!, $idProject: String!){
    CreateReaction(title: $title, idUser: $idUser, idProject: $idProject) {
      title
    }
  }
`;

export const CREATE_PROJECT = gql`
  mutation CreateProject($title: String!, $description: String!, $idUser: String!){
    CreateProject(title: $title, description: $description,  idUser: $idUser) {
      title
      description
    }
  }
`;
