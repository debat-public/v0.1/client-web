import React, { useState } from "react";
import Project from "../components/Project/Project";
import ListOpinions from "../components/ListOpinions/ListOpinions";
import SideMenu from "../components/Project/SideMenu";
import listBlocksJSON from "../json/listblocks.json";
import { GET_BLOCKS } from "../graphql/queries";
import { useQuery } from "@apollo/react-hooks";
import MenuReaction from "../components/Project/MenuReaction";
import listReactionsJSON from "../json/listreactions.json";
import listOptionsJSON from "../json/listoptions.json";
// import { d } from "../../graphql/queries";

/**
 * View to display the project page
 */
function ProjectPage(): React.ReactElement {
  const { loading, error, data } = useQuery(GET_BLOCKS);
  const [listBlocks, setListBlocks] = useState<Array<any>>(
    data?.ListBlocks || listBlocksJSON
  );

  return (
    <section className="section  has-background-grey-lighter" style={{ height: "100%" }} >
      <div className="columns" style={{ height: "100%" }}>
        <div className="column has-background-white is-narrow">
          <MenuReaction listReactions={listReactionsJSON} title="Réactions" />
          <MenuReaction listReactions={listOptionsJSON} title="Options" />
        </div>
        <div className="column" style={{ overflowY: "auto" }}>
          <Project />
          <ListOpinions />
        </div>
        <div className="column has-background-white is-narrow">
          <SideMenu list={listBlocks} />
        </div>
      </div>
    </section>
  );
}
export default ProjectPage;
