import React from "react";
import { connect } from 'react-redux';
// import { Redirect } from 'react-router-dom';
import InfoPanel, { StatusKey } from "../components/Profile/InfoPanel";
import MenuProfile from "../components/Profile/MenuProfile";


/**
 * Views to display the profile page
 * @param props 
 */
function ProfilePage(props: any): React.ReactElement {
  const { user: { firstname, lastname, bio } } = props;
  const search = props.location.search.substring(7) ? props.location.search.substring(7) : StatusKey.DEFAULT; // Delete ?panel=
  const id = props.match.params.id;

  // if (!firstname) return <Redirect to='/'/>;
  return (
    <section className="section has-background-grey-lighter" id="profile">
      <div className="container is-fluid has-text-centered">
        <div className="card">
          <div className="card-content">
          <MenuProfile statusKey={search} />
          <InfoPanel statusKey={search} />
          </div>
        </div>
      </div>
    </section>
  );
}
const stateToProps = (store: any) => ({
  user: store.user,
})
export default connect(stateToProps)(ProfilePage);
