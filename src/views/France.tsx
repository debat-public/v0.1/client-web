import React, { Component } from "react";
import * as d3 from "d3";
import franceGeo from "../json/franceGeo.json";
import { Redirect } from "react-router-dom";

class FranceMap extends Component<any> {
  componentDidMount() {
    this.makeChart();
  }

  state = {
    to: null,
  }

  makeChart() {
    const { id, width, height } = this.props;
    const path: any = d3.geoPath();
    const goTo = (to: string) => this.setState({to});

    const tooltipDiv = d3
      .select("body")
      .append("div")
      .attr("class", "tooltip")
      .style("opacity", 0);

    const projection = d3
      .geoConicConformal()
      .center([2.454071, 46.279229])
      .scale(2600)
      .translate([width / 2, height / 2]);

    path.projection(projection);

    const svg = d3
      .select("#" + id)
      .append("svg")
      .attr("width", width)
      .attr("height", height);

    const deps = svg.append("g");

    deps
      .selectAll("path")
      .data(franceGeo.features)
      .enter()
      .append("path")
      .attr("class", "department")
      .attr("d", path)
      .on("mouseover", function (d) {
        tooltipDiv.transition().duration(200).style("opacity", 0.9);
        tooltipDiv
          .html(
            "Département : " +
              d.properties.NOM_DEPT +
              "<br/>" +
              "Région : " +
              d.properties.NOM_REGION
          )
          .style("left", d3.event.pageX + 30 + "px")
          .style("top", d3.event.pageY - 30 + "px");
      })
      .on("mouseout", function (d) {
        tooltipDiv.style("opacity", 0);
        tooltipDiv.html("").style("left", "-500px").style("top", "-500px");
      })
      .on('click', function(d){
        tooltipDiv.style("opacity", 0);
        tooltipDiv.html("").style("left", "-500px").style("top", "-500px");
        const location = '/france/' + d.properties.NOM_DEPT;
        goTo(location)
      });
  }

  render() {
    const { to } = this.state;

    if (to === null) return (
      <div
        id={this.props.id}
        style={{
          textAlign: "center",
          minHeight: "700px",
          padding: "50px",
        }}
      ></div>
    );
    return <Redirect to={to!} />
  }
}

export default FranceMap;
