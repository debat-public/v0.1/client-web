import React, { useState } from "react";
import ListCategories from "../components/Home/ListCategories";
import ListProjects from "../components/ListProjects/ListProjects";


function HomePage(): React.ReactElement {
  return (
    <div>
      <section className="section has-background-grey-lighter">
        <div className="hero">
          <div className="hero-body">
            <h1 className="title has-text-centered" id="top">
              Bienvenue sur debat.gouv.fr
            </h1>
            <h2 className="subtitle has-text-centered">
              Vous donnez la parole et la faire entendre
            </h2>
            <h3 className="has-text-centered">
              Code source :
              <a href="https://gitlab.com/debat-public">
                gitlab.com/debat-public
              </a>
            </h3>
          </div>
        </div>
      </section>
      <div className="columns">
        <div className="column is-one-fifth">
          <ListCategories />
        </div>
        <div className="column">
          <ListProjects />
        </div>
      </div>
    </div>
  );
}

export default HomePage;
