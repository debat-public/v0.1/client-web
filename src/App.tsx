import React, { ReactElement } from "react";
import { connect } from "react-redux";
import Routes from "./Routes";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";

import Connexion from "./services/connexion";

function App(): ReactElement {
  return (
    <Connexion style={{ height: "100%" }}>
      <Navbar />
      <Routes />
      <Footer />
    </Connexion>
  );
}

export default connect((store) => store)(App);
