import React from "react";

interface StepsInterface {
    idActiveStep?: number;
}


function Steps(props: StepsInterface): React.ReactElement {
    const { idActiveStep } = props;
    /**
     * list of status:
     *      done
     *      active
     *      null
     */
    const steps = [
        {
            "status": "active",
            "name": "Préparation"
        },
        {
            "status": "done",
            "name": "Débat"
        },
        {
            "status": "",
            "name": "Réalisation"
        },
        {
            "status": "",
            "name": "Réalisation"
        },
    ];

    return (
        <ul className="steps-form">
            {steps.map((el: any, i: number) => (
                 <li  key={`item-${i}`} className={el.status}>
                 <div>
                     {el.name}
                 </div>
             </li>
            ))}
        </ul>
    );
}
export default Steps;


