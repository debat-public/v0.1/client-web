import React, { useState } from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { CREATE_REACTION } from "../../graphql/mutations";

/**
 * Component ListReations
 */

interface Reaction {
  count: number,
  title: string,
}

interface MenuReactionInterface {
  listReactions: Array<Reaction>
  title: string
}

function MenuReaction(props: MenuReactionInterface): React.ReactElement {
  const { listReactions, title } = props;
//   const [saveRocket, { error, data }] = useMutation<
// { saveRocket: RocketInventory },
// { rocket: NewRocketDetails }
// >(CREATE_REACTION, {
// variables: {  title: "data"}
// });

  return (
    <aside className="panel mt-6">
      <p className="panel-heading">{title}</p>
      <ul className="panel-list">
        {listReactions.map(
          (e: { title: string, count: number }, i: number) => (
            <a className="panel-block is-active" key={i} onClick={() => {
              // useMutation()
            }}>
              <span > {e.title} </span>
              <span className="panel-icon mr-6"> {e.count}</span>
            </a>
          )
        )}
      </ul>
    </aside>
  );
}
export default MenuReaction;
