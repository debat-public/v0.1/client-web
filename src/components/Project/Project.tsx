import React, { Fragment, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import ProjectInterface from "../../interfaces/Project.interface";
import PreviewBloc from "./PreviewBloc";
import EditBlock from "./EditBlock";
import Steps from "../Project/Steps";
import { GET_BLOCKS } from "../../graphql/queries";
import listBlocksJSON from "../../json/listblocks.json";

interface blockInterface { body: string, title: string, edit_user: boolean }

function Block(props: blockInterface): React.ReactElement {
  const [show, setShow] = useState(false)
  let Title = <h1>{props.title}</h1>;
  let containerBlock =  null;

  const close = () => setShow(false)

  if (props.edit_user) {
    Title = <div> <h1>{props.title}</h1> <button onClick={() => setShow(true)}> test</button></div>;
    
    containerBlock = <EditBlock  mardownContent={props.body} title={"titre"} show={show} onHide={close} />
  }
  return (
    <Fragment>
      {Title}
       <PreviewBloc body={props.body} />
      {containerBlock}
    </Fragment>
  );
}



function Project(projectData: ProjectInterface): React.ReactElement {
  const { loading, error, data } = useQuery(GET_BLOCKS);
  const [listBlocks, setListBlocks] = useState<Array<any>>(data?.ListBlocks || listBlocksJSON);



  return (
    <div className="card">
      <header className="card-header">
        <p className="card-header-title">
          Projet Cigéo - bilan du garant de la concertation
        </p>
        <p className="card-content">Crée le 1 Juin 2020</p>
      </header>
      <br />
      <Steps />
      <div className="card-content">
        <div className="content">
          {listBlocks.map(
            (e: { title: string, body: string }, i: number) => (
              <div key={i}>
                {/* <h1>{e.title}</h1> */}
                <Block body={e.body} title={e.title} edit_user={true} />
                {/* <PreviewBloc body={e.body} /> */}
              </div>
            )
          )}
        </div>
      </div>
    </div>
  );
}
export default Project;
