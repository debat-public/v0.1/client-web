import React, { ReactElement } from "react";
import listBlocksJSON from "../../json/listblocks.json";

interface SideMenuInterface {
  list: any;
}

function SideMenu(props: SideMenuInterface): ReactElement {
  const list = listBlocksJSON;
  return (
    <aside className="menu">
      <p className="menu-label">Projet</p>
      <ul className="menu-list">
        {list.map((el, i) => (
          <li>
            <a href={`#${el.title.replace(/\s+/g, '-').toLowerCase()}`}> {el.title}</a>
          </li>
        ))}
      </ul>
    </aside>
  );
}

export default SideMenu;
