import React, { useState } from "react";

const Login = (props: any) => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const changeEmail = ({ target: { value } }: any) => setEmail(value);
  const changePassword = ({ target: { value } }: any) => setPassword(value);
  return (
    <section className="hero">
      <div className="hero-body">
        <div className="container">
          <div className="columns is-centered">
            <div className="column is-5-tablet is-4-desktop is-3-widescreen">
              <form action="" className="box">
                <div className="field">
                  <div className="control">
                    <input
                      type="email"
                      name="email"
                      placeholder="Email"
                      style={{ marginBottom: 5 }}
                      onChange={changeEmail}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input
                      type="password"
                      name="password"
                      placeholder="Password"
                      style={{ marginBottom: 5 }}
                      onChange={changePassword}
                    />
                  </div>
                  <div className="field">
                    <button className="button is-primary">Login</button>
                  </div>
                  <a href="###" style={{ float: "right", marginRight: 0 }}>
                    Mot de passe oublié ?
                  </a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Login;
