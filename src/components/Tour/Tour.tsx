import React, { useState } from "react";
import Tour from "reactour";

export const steps = [
  {
    selector: ".title",
    content: "test",
  },
  {
    selector: ".container",
    content: "test",
  },

  // ...
];

/**
 * Component to make a tour of the App
 * Configure steps to add or to delete info
 */
function HomePageTour(): React.ReactElement {
  const [isTourOpen, setIsTourOpen] = useState(true);
  return (
    <Tour
      steps={steps}
      showNumber={false}
      rounded={50}
      isOpen={isTourOpen}
      onRequestClose={() => setIsTourOpen(false)}
    />
  );
}

export default HomePageTour;
