import React from "react";
import { useQuery } from "@apollo/react-hooks";
import listFilesJSON from '../../json/listfiles.json';
import FilePreview from "./FilePreview";
import { GET_FILES } from '../../graphql/queries';

/**
 * Component ListFiles
 */
function ListFiles(): React.ReactElement {
    const { loading, error, data } = useQuery(GET_FILES);
    console.table({ loading, error, data });
    let datalistFiles: any;
    if (loading) return <div>loading</div>;
    else if (error) {
        console.error(error);
        datalistFiles = listFilesJSON;
    }
    else {
        datalistFiles = data.listFiles;
    }

    return (
        <div className="cards">
            {/* Liste des réactions */}
            <div className="row">
                {datalistFiles.map(
                    (e: { title: string }, i: number) => (
                        <FilePreview title={e.title} key={i} />
                    )
                )}
            </div>
        </div>
    );
}
export default ListFiles;

