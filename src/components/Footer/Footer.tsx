import React, { ReactElement } from "react";

function Footer(): ReactElement {
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          <strong>Débat public</strong> site réalisé pour la 
          <a href="https://jgthms.com"> CNDP</a>. 
          Sous license 
          <a href="http://opensource.org/licenses/mit-license.php"> MIT</a>
          <a href="https://gitlab.com/debat-public"> code source</a>
        </p>
      </div>
    </footer>
  );
}

export default Footer;
