import React, { useState } from "react";
import OpinionPreview from "./OpinionPreview";
import { useQuery } from "@apollo/react-hooks";
import listOpinionsJSON from "../../json/listopinions.json";
import { GET_OPINIONS } from "../../graphql/queries";
import { CREATE_REACTION } from "../../graphql/mutations";

function ListOpinions(): React.ReactElement {
  const { loading, error, data } = useQuery(GET_OPINIONS);
  console.table({ loading, error, data });
  const [listOpinions, setListlistOpinions] = useState<Array<any>>(data?.ListOpinions || listOpinionsJSON);

  return (
    <section className="section">
      <div className="field">
        <label className="label is-small">Order by:</label>
        <p className="control has-icons-left">
          <span className="select">
            <select>
              <option selected>Date</option>
              <option>Nombre de soutiens</option>
              <option>Nombre de réactions</option>
            </select>
          </span>
          <span className="icon is-small is-left">
            <i className="fas fa-globe"></i>
          </span>
        </p>
      </div>
      <div className="cards">
        <div className="row">
          {listOpinions.map(
            (e: { title: string; description: string }, i: number) => (
              <OpinionPreview
                title={e.title}
                description={e.description}
                key={i}
              />
            )
          )}
        </div>
      </div>
    </section>
  );
}
export default ListOpinions;
