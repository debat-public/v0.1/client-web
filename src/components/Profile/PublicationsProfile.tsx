import React from "react";
import ProfileInterface from "../../interfaces/Profile.interface";
import ListProjects from "../ListProjects/ListProjects";

function PublicationsProfile(props: ProfileInterface): React.ReactElement {
  return (
    <div>
      <ListProjects />
    </div>
  );
}

export default PublicationsProfile;
