<!DOCTYPE html>
<html lang="en" class="has-navbar-fixed-top">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Template debat public</title>
  <link rel="stylesheet" href="css/mystyles.css" />
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script type="text/javascript" src="index.js"></script>
</head>

<body>
  <div className="modal is-active is-clipped">
    trest modal
  </div>
  <div id="navbar"></div>
  <?php include("src/navbar.html");  ?>
  <div id="section-0"></div>
  <?php include("src/section-0.html");  ?>
  <div id="section-1"></div>
  <?php include("src/section-1.html");  ?>
  <div id="section-2"></div>
  <?php include("src/section-2.html");  ?>
  <div id="section-3"></div>
  <?php include("src/section-3.html");  ?>
  <div id="section-typography"></div>
  <?php include("src/section-typhography.html");  ?>

  <div id="section-color"></div>
  <?php include("src/section-color.html");  ?>

  <div id="navbar"></div>
  <?php include("src/navbar.html");  ?>
  <section class="section is-large" id="project">
    <div id="section-steps"></div>
    <?php include("src/section-steps.html");  ?>
    <div id="section-project"></div>
    <?php include("src/page-project.php");  ?>
  </section>
  <?php include("src/footer.html");  ?>


  <div id="navbar"></div>
  <?php include("src/navbar.html");  ?>
  <div id="section-dashboard"></div>
  <?php include("src/page-dashboard.html");  ?>

  <div id="navbar"></div>
  <?php include("src/navbar.html");  ?>
  <div id="section-profile"></div>
  <?php include("src/page-profile.html");  ?>


</html>