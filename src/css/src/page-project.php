<section class="container is-fluid">
  <div class="columns">
    <div class="column">
      <div class="card">
        <header class="card-header">
          <p class="card-header-title">
            Projet Cigéo - bilan du garant de la concertation
          </p>
          <p class="card-content">
            Crée le 1 Juin 2020
          </p>
          <a href="#" class="card-header-icon" aria-label="more options">
            <span class="icon">
              <i class="fas fa-angle-down" aria-hidden="true"></i>
            </span>
          </a>
        </header>
        <div class="card-content">
          <div class="content">
            <h1>Hello World</h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
              accumsan, metus ultrices eleifend gravida, nulla nunc varius lectus,
              nec rutrum justo nibh eu lectus. Ut vulputate semper dui. Fusce erat
              odio, sollicitudin vel erat vel, interdum mattis neque.
            </p>
            <div id="contexte">
              <h1>
                Contexte du projet
              </h1>
              <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>One</th>
                    <th>Two</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Three</td>
                    <td>Four</td>
                  </tr>
                  <tr>
                    <td>Five</td>
                    <td>Six</td>
                  </tr>
                  <tr>
                    <td>Seven</td>
                    <td>Eight</td>
                  </tr>
                  <tr>
                    <td>Nine</td>
                    <td>Ten</td>
                  </tr>
                  <tr>
                    <td>Eleven</td>
                    <td>Twelve</td>
                  </tr>
                </tbody>
              </table>

              <div class="content is-large">
                <h2>Second level</h2>
                <p>
                  Curabitur accumsan turpis pharetra
                  <strong>augue tincidunt</strong> blandit. Quisque condimentum
                  maximus mi, sit amet commodo arcu rutrum id. Proin pretium urna
                  vel cursus venenatis. Suspendisse potenti. Etiam mattis sem
                  rhoncus lacus dapibus facilisis. Donec at dignissim dui. Ut et
                  neque nisl.
                </p>
                <ul>
                  <li>
                    In fermentum leo eu lectus mollis, quis dictum mi aliquet.
                  </li>
                  <li>
                    Morbi eu nulla lobortis, lobortis est in, fringilla felis.
                  </li>
                  <li>
                    Aliquam nec felis in sapien venenatis viverra fermentum nec
                    lectus.
                  </li>
                  <li>Ut non enim metus.</li>
                </ul>
              </div>

              <p>
                TODO creer un exemple de table | et d'insertion d'image TODO
                reflechier a formater ce texte. Couleur font saut a la ligne
                etc... se renseigner sur le format sortant de draftJS Les
                premières réflexions autour d'une liaison Fos-Salon émergent dans
                les années 1960, au moment de l'aménagement du Port Autonome de
                Marseille et de la zone industrialo-portuaire (ZIP) de
                Fos-sur-Mer. Les études relatives à une liaison A56 entre l'A55
                (Marseille / Martigues) et l'A54 (Salon-de-Provence / Arles) sont
                lancées en 1970 et cet aménagement est déclaré d’utilité publique
                en novembre 1976. Cette liaison sera partiellement réalisée dans
                les années 80 lors de la construction de la RN569 en chaussée
                bi-directionnelle. Le dynamisme économique du pourtour de l’étang
                de Berre(plateforme logistique Clésud à Miramas, plateforme
                maritime Fos 2XL au sein Grand Port Maritime de Marseille(GPMM),
                pôle aéronautique et zone logistique militaire à Istres) a relancé
                le projet d’une liaison routière offrant un plus haut niveau de
                service. Le projet de liaison Fos - Salon consiste en
                l’aménagement d’une infrastructure routière d’environ 25km
                permettant d’améliorer la connexion de la zone portuaire à
                l’autoroute A54 et la desserte locale du territoire.La réalisation
                d'ici 2030 de cette liaison routière est l'un des objectifs fixés
                par la Commission « Mobilité 21 » en 2013, objectif confirmé par
                le Conseil d'Orientation des Infrastructures (COI) en 2018. Elle
                figure dans les projets cités dans l’exposé des motifs de la Loi
                d’Orientation sur les Mobilités, définitivement adoptée le 19
                novembre 2019. Trois grandes options d'aménagement ont été
                étudiées à ce stade : Une option « historique » qui consiste à
                réaliser une infrastructure autoroutière; Deux options issues des
                recommandations du Conseil d'Orientation des Infrastructures : La
                première dite « autoroutière intermédiaire »; La seconde dite «
                voie express ».
              </p>
            </div>
            <h1>Descriptif du projet</h1>
            <div id="obj">
              <h1> Objectifs poursuivis par le projet</h1>
              <p>
                ( Defnition des objectifs / de la roadMap du projet ) TODO se
                renseinger sur un visualisateur de ROADMAP. Le projet de Liaison
                Fos-Salon poursuit quatre objectifs répondant aux enjeux du
                territoire en matière de mobilité et de déplacement: Desservir la
                ZIP et le port de Fos - sur - Mer avec un niveau de service
                performant Desservir de manière optimisée le territoire en
                intégrant une composante multimodale Augmenter le niveau de
                sécurité routière du réseau pour les usagers Réduire les nuisances
                aux populations et les impacts sur le cadre de vie.
              </p>
            </div>
            <div id="maitre-ouvrage">
              <h1>Le dossier du maître d'ouvrage </h1>
              <ul>
                <div class="card">
                  <div class="card-content is-inline-flex">
                    <li>
                      <div class="content">
                        <a>Compte-rendu.pdf</a>
                        <button>Telecharger</button>
                      </div>
                    </li>
                  </div>
                </div>
                <div class="card">
                  <div class="card-content is-inline-flex">
                    <li>
                      <div class="content">
                        <a>Compte-rendu.pdf</a>
                        <button>Telecharger</button>
                      </div>
                    </li>
                  </div>
                </div>
                <div class="card">
                  <div class="card-content is-inline-flex">
                    <li>
                      <div class="content">
                        <a>Compte-rendu.pdf</a>
                        <button>Telecharger</button>
                      </div>
                    </li>
                  </div>
                </div>
              </ul>
              <a href="https://fos-salon.debatpublic.fr/images/documents/dmo/dmo-fos-salon.pdf">Consultez le dossier du maître d'ouvrage</a>
              <a href="https://fos-salon.debatpublic.fr/images/documents/dmo/dmo-synthese.pdf">Consultez la synthèse du dossier de saisine</a>
            </div>
            <div id="localisation">
              <h1> Le projet soumis à débat | TODO changer de titre par rapport a la
                localisation ?</h1>

              <p>
                Situé sur les communes de Salon - de - Provence, Grans, Miramas,
                Istres et Fos - sur - Mer, l’aménagement de la liaison routière a
                vocation à: - Améliorer la liaison entre les différents pôles
                urbains de Fos, Istres, Miramas et Salon - de - Provence,
                organisés en chapelet le long de la RN569, en offrant une desserte
                de tous les « pôles générateurs de déplacements » via de nombreux
                échangeurs; - Développer l’intermodalité en incitant à l’adoption
                de moyens alternatifs à l’utilisation de la voiture individuelle
                en proposant des points de connexions avec les différents
                transports en commun; - Contribuer à accroitre la compétitivité du
                GPMM et accompagner le développement de la ZIP en leur assurant
                une desserte performante(depuis le nord via l'A54, depuis l'est
                via l'A55) et en améliorant significativement les connexions entre
                le port, sa couronne logistique de proximité et son hinterland
                vers le couloir rhodanien et l’arc méditerranéen ; - Fluidifier et
                améliorer la sécurité de la circulation sur cet axe majeur ; -
                Réduire les nuisances aux populations et les impacts sur le cadre
                de vie et l’environnement.
              </p>
            </div>
            <div id="options">
              <h1>Listes des options</h1>

              <div class="card">
                <div class="card-content is-inline-flex">
                  <div class="content">
                    <label class="checkbox">
                      <input type="checkbox" />
                      Option 1
                    </label>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-content is-inline-flex">
                  <div class="content">
                    <label class="checkbox">
                      <input type="checkbox" />
                      Option 2
                    </label>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-content is-inline-flex">
                  <div class="content">
                    <label class="checkbox">
                      <input type="checkbox" />
                      Option 3
                    </label>
                  </div>
                </div>
              </div>
              TODO Creer un outil pour definir les options | list bullets ? Trois
              grandes options d’aménagement ont été étudiées à ce stade : L’option
              autoroutière consiste à aménager la RN569 en autoroute urbaine à 2x2
              voies permettant une vitesse de 110 km/h, abaissée à 90 km/h au
              droit des zones urbaines, ainsi qu’un nouveau contournement à 2x2
              voies de Fos-sur-Mer ; L’option autoroutière intermédiaire consiste
              à aménager la RN569 en infrastructure de type autoroutier à 2x2
              voies avec des échangeurs dénivelés entre l’A54 et l’échangeur
              Dassault d’Istres sud. La section au sud serait à 2x1 voie entre
              Dassault et la RN568 et le contournement existant de Fos-sur-Mer
              serait amélioré ; L’option voie express consiste à aménager la RN569
              en voie express à 2x1 voie, avec créneaux de dépassements et
              carrefours dénivelés au droit des secteurs présentant les plus forts
              trafics, et à aménager le contournement existant de Fos-sur-Mer.
            </div>

            <div id="cout">
              <h1>Couts</h1>
              <p>
                TODO Tableau | schema ? Representation permetant de mettre en avant
                les chiffres en rajouter les financements Trois grandes options
                d’aménagement ont été étudiées à ce stade : L’option autoroutière
                consiste à aménager la RN569 en autoroute urbaine à 2x2 voies
                permettant une vitesse de 110 km/h, abaissée à 90 km/h au droit des
                zones urbaines, ainsi qu’un nouveau contournement à 2x2 voies de
                Fos-sur-Mer ; L’option autoroutière intermédiaire consiste à
                aménager la RN569 en infrastructure de type autoroutier à 2x2 voies
                avec des échangeurs dénivelés entre l’A54 et l’échangeur Dassault
                d’Istres sud. La section au sud serait à 2x1 voie entre Dassault et
                la RN568 et le contournement existant de Fos-sur-Mer serait amélioré
                ; L’option voie express consiste à aménager la RN569 en voie express
                à 2x1 voie, avec créneaux de dépassements et carrefours dénivelés au
                droit des secteurs présentant les plus forts trafics, et à aménager
                le contournement existant de Fos-sur-Mer.
              </p>
            </div>

            <div id="maitre-ouvrage">
              <h1> Le maître d’ouvrage </h1>
                <p>
                Le nom du responsable direct a la creation du projet un petit recap
                de l'equipe avec qui il fait ç et la liste de tout les personnes
                travaillant dessus( responsable aussi / garants etc..)
                </p>
            </div>

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec
            iaculis mauris.
            <a href="#">@bulmaio</a>. <a href="#">#css</a>
            <a href="#">#responsive</a>
            <br />
          </div>
        </div>
        <footer class="card-footer buttons">
          <button class="button">
            <span>J'approuve</span>
            <span class="icon">
              24
            </span>
          </button>
          <button class="button">
            <span>Je désapprouvre</span>
            <span class="icon">
              10
            </span>
          </button>
          <button class="button">
            <span>Neutre</span>
            <span class="icon">
              7
            </span>
          </button>
          <button class="button">
            <span>Option 1</span>
            <span class="icon">
              10
            </span>
          </button>
          <button class="button card-footer-item">
            <span>Option 2</span>
            <span class="icon">
              7
            </span>
          </button>
          <button class="button card-footer-item">
            <span>Option 3</span>
            <span class="icon">
              10
            </span>
          </button>
          <button class="button card-footer-item">
            <span>Option 4</span>
            <span class="icon">
              7
            </span>
          </button>
          <button class="button card-footer-item">
            <span>Option 5</span>
            <span class="icon">
            </span>
          </button>
        </footer>
      </div>
      <div class="section is-large has-background-grey-lighter">
        <?php include("src/opinions.php");  ?>
        <?php include("src/form-opinion.html");  ?>
      </div>
    </div>

    <div class="column">
      <aside class="menu">
        <p class="menu-label">
          Projet
        </p>
        <ul class="menu-list">
          <li><a href="#contexte">Contexte du projet</a></li>
          <li><a href="#objectifs">Objectifs poursuivis par le projet</a></li>
          <li><a href="#dossier-maitre-ouvrage">Le dossier du maître d'ouvrage</a></li>
          <li><a href="#debat">Le projet soumis à débat</a></li>
          <li><a href="#couts">Couts</a></li>
          <li><a href="#maitre-ouvrage">Le maître d’ouvrage</a></li>
        </ul>
      </aside>
    </div>
  </div>
</section>