import React, { useState } from "react";
import { connect } from "react-redux";
import { useLocation } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { GET_USER } from '../graphql/queries';
import { ProvideUserQueryVariables, User } from "../generated/graphql";

const makeState = () => {
  let result           = '';
  let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;
  for ( let i = 0; i < 42; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

const getVariables = (code: string): ProvideUserQueryVariables => {
  return {
    code,
    state: makeState(),
    provider: "42",
  };
};

interface AuthResponse {
  response: boolean;
  next?: string;
  data?: any;
}

const authentication = (search: string): AuthResponse =>  {
  const location = new URLSearchParams(search);
  const code = location.get("code");
  const previousCode = localStorage.getItem("code");

  if (code && code !== previousCode) {
    localStorage.setItem("code", code);
    return {
      response: true,
      next: "mutate",
      data: { variables: getVariables(code) },
    };
  } else {
    return { response: false };
  }
}

const Connexion = (props: any) => {
  const { user } = props;
  const { search } = useLocation();
  const { data: dataAuth } = authentication(search);
  const [variables] = useState<ProvideUserQueryVariables | null>(
    dataAuth?.variables ? dataAuth.variables : null
  );
  const { error, data, loading } = useQuery(GET_USER, {
    variables,
  });
  const userData: User = data?.provideUser || null;

  if (userData?.id && !user?.id) {
    localStorage.setItem("user", JSON.stringify(userData));
    window.location.href = "/";
  }
  console.log(data, user, props);

  return <div>{props.children}</div>;
}

export default connect((store) => store)(Connexion);
